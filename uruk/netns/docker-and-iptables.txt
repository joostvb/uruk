# Based upon https://fruit.je/docker-and-iptables by
# Wessel Dankers <webmaster@fruit.je>, 2024.
# License: WTFPL version 2, see file WTFPL-2 or
# http://www.wtfpl.net/txt/copying/

How to make Docker keep its grubby hands off of your iptables rulebase
======================================================================

Running Docker is awesome and all, but it assumes it can do a hostile takeover
of your iptables rulebase and sometimes that's a problem. This script and
accompanying systemd unit will move Docker to its own Linux kernel network
namespace, so it can do all the iptables trickery it wants but your host
iptables rulebase will stay out of reach.

Of course that does mean you'll probably have to set up some iptables rules
yourself, especially if you want to publish services from your host. The script
prints the necessary lines to give Docker access to the internet (you can find
the output using journalctl).

There are many possible variations on this concept, for example one might use a
bridge device to give Docker its own presence on the local network, independent
of the host. This script is ultimately just a starting point.

Caveat
------

As with all scripts you find on the internet: if you don't understand what it
does and how, this script is not for you. Read it carefully, make sure you
understand all parts and how it fits into your situation.

How to install
--------------

The uruk Debian package ships the main script as /usr/lib/uruk/docker-netns.

In order to enable the docket-netns service file, run, as root:

 ln -s /usr/share/doc/uruk/examples/docker-netns.service /etc/systemd/system/docker-netns.service
 mkdir -p /etc/systemd/system/docker.service.d
 ln -s /usr/share/doc/uruk/examples/docker-override.conf /etc/systemd/system/docker.service.d/override.conf

Then, run:

 systemctl daemon-reload
 systemctl restart docker

